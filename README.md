<div style="display: flex; 
    flex-direction: column; 
    justify-content: center; 
    align-items: center;">
    <img style="width: 10em;" src="http://breath57.gitee.io/vs-docs/images/vs-logo.ico"/>
</div>
<h1 style="text-align: center;">VSearcher</h1>
<p style="text-align: center;">国内为数不多的教学类视频内容检索Python库</p>
<div style="display: flex;
    flex-direction: row;
    justify-content: center;
}">
<img style="margin: 0 .27em;
    margin: .2em;" src='https://img.shields.io/badge/最新版本-v0.2.16-3963bc.svg'/>
<img style="margin: 0 .27em;
    margin: .2em;" src='https://img.shields.io/badge/build-passing-00d508.svg'/>
<img style="margin: 0 .27em;
    margin: .2em;" src='https://img.shields.io/badge/license-MIT-f0f900.svg'/>
</div>
<div style='display: flex;
    flex-direction: row;
    justify-content: center;
}r'>
    <img style="margin: 0 .27em;
    margin: .2em;" src='https://img.shields.io/badge/python-v3.8+-7efaff.svg'/>
    <img style="margin: 0 .27em;
    margin: .2em;" src='https://img.shields.io/badge/setuptools-v59.8.0+-7ecaff.svg'/>
    <img style="margin: 0 .27em;
    margin: .2em;" src='https://img.shields.io/badge/pip-v21.3.1+-00d5a8.svg'/>
    <img style="margin: 0 .27em;
    margin: .2em;" src='https://img.shields.io/badge/opencv_python-v4.5.5-0fcff8.svg'/>
</div>


<div style='display: flex;
    flex-direction: row;
    justify-content: center;
    align-items: center;;margin: .5em 0  3em 0;'>
    <a style="   
    border-radius: 5px;
    width: 8em;
    font-size: 1.3em;
    text-align: center;
    line-height: 2em;
    height:2em;
    text-decoration-line: none;
    background-color: #1387c1;
    color: #fff;
    margin: 0.2em 1em;
    padding: 0 0.2em;
    text-align: center;
    " href="http://breath57.gitee.io/vs-docs/">查看官方文档</a>
</div>
