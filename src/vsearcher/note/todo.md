### 设计Vsearch对象负责初始化的操作, 就是负责相关的配置
### 思路2: 变为flask_vsearch, 需要传入 app 也就是flask核心对象


### 应该分成  单独的视频和目录更好， 就是无限层级 生成相应的资源

视频 | 目录

数据库表： 视频表 | 目录表
    一个目录 -》 多个目录 | 多个视频

```py
class Video:
    name
    url
    local_url
    dir_id

class Dir:
    name
    dir_id

    videos = 
    dirs = 

@WAIT 一个目录既有视频又有视频呢, 那这个DIR的表解构，只需要查 dir——id属于的就可以，或者增加一个字段,type: 1只有目录 2只有视频 3两种都有，如果是2，就去只查视频表

@WAIT VS核心库也需要发生变化
递归的生成o_path对象，都是可行的，这样业务逻辑少了很多，扩展性也更强
```


2. 注释更佳的完善和完整
    1) 二次使用的过程中
    2) 直接重新阅读代码, 理解源码



2. 引入多线程的机制


@RISKED %作为分隔符号, 在浏览器中打开图片有bug


3. (不好意思, 这好像是服务器要做的, 不是这个库该干的事, 哈哈哈)传入的文件, 全为文件名, 而不是唯一的ID, 因此需要接入数据库来维护, 定义每个视频的唯一性


4. 对于返回的PDF可以处理为可搜索的pdf

5. 图片可以进行清晰化的处理, 可以给用户自定义选择, 这样增加等待时间, 用户也能够接收


6. 程序可以同时兼容, 本地模式和网络模式
    网络模式: 就是提供视频的URL路径, 然后等待服务器处理完毕, 就可
    本地模式: 可以通过UI界面选择文件目录进行提取PDF, 或者搜索视频的内容


7. pdf的文件内容有点大, 可以进行适当的压缩
    压缩的方案: 1. PDF压缩 2. 保存图片前进行压缩


8. 线程池的优化进行改进，目前为每执行一个任务都用到了线程池，如果有一个全局的且类似Java中的newCachedThreadPool的话，可以节约线程频繁创建销毁的开销。当前如果有10个线程请求调用executeVideo方法，将会产生10*th_thread_num个线程，可能造成内存溢出以及性能上的问题。